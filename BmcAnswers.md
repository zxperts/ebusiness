# EnlightMe 

## 1. Proposition de valeur
---
**EnLightME** est un projet de lutte contre la criminalité et d'économies d'energie mettant en oeurvres
plusieurs études liant l'éclairage public (l'intensité ou absence) au nombre de délits commis.

[Reportage levif.be](http://www.levif.be/actualite/a-los-angeles-la-criminalite-varie-selon-l-intensite-de-l-eclairage-public-video/article-normal-389567.html)
(trouver d'autres liens)...

- Données démographique des opérateurs téléphoniques + système IA (population nomade/sédentaire)
- Modules permettant la variation de l'eclairage public
- lampe LED

```java
luminosite réelle = luminosité de base * (population nomade / population sédentaire)
```
### Bénéfice pour le client
- Sécurisation de la ville
- Economie de +/- 40% des consommations electriques pour l'éclairage public.

## 2. Les segments de la clientèle
---
Ce projet s'adresse à des **villes** de minimum 100 000 habitants (Liège, Namur, ...).

## 3. Les canaux
---
Démarchage agressif des organes de gestion des grandes villes... (pays, continent)
Mise en place de programme pilote à l'echelle d'une commune/d'un quartier pour validation d'un projet à l'echelle de la ville.
- Accompagnement par des ingénieur conseil (calcul coût, économie, ...)
- Formation des technicien de la ville pour remplacement des module de commande de l'eclairage.
- accompagnement mise en place système IA

## 4. La relation client
---
voir point 3.
- Accompagnement lors de la mise en place du projet
- Suivi de l'état de l'IA
- provisionnement quotidien des données de l'IA
- Support en cas de panne IA (heure de bureau)
- Apporvisionnement stock module electrique.

**Quelle relation voulez-vous entrenir
avec les clients ?**
> Relation personnalisée avec le client
 
**Mécanismes pour acquérir les clients (références, certificats et labels, ...)**
> Démarchage agréssif des organes de gestion des villes (pays, continent, mondial)

**Mécanismes pour retenir le clients (carte de membre, carte de fidélité, ...)**
> Garder les données démographiques @home et ne fournir que l'IA pour Interprétation des données et commande des modules électrique.

**Mécanismes pour faire acheter plus (suggestions, newsletters, ...)**
> _Elargir le système pour calcul des fréquences de collecte des déchets ménagers?_

## 5. Source de revenu
---
Vente du module
Location système IA
Contrat annuel maintenance (Support en cas de panne, mise à jour logiciel)


## 6. Les ressources clefs
---
- Ingénieurs commerciaux
- Analyste développeurs/systèmes
- Actuaire/Statisticien
- Capacité stockages données démographiques

## 7. Les activités clefs
---
- informatique
- Marketing, Vente
- Approvisionnement 

***(Voir Configurateur de valeur 21/25)***

## 8. Partenariat clefs
---
- société télécom (principalement GSM)
- fabricant module electrique
- fournisseur LED
- société de livraison

## 9. Coûts et profit
---
Grande question existencielle... :-)